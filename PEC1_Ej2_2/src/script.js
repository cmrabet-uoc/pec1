/**
 * Reserva de asientos de cine.
 * Script principal.
 * Chakir Mrabet <cmrabet@uoc.edu>, 2020.
 * PEC 1/2, Desarrollo fron-end con frameworks JavaScript.
 * Master en desarrollo de sitios y applicaciones web, UOC.
 */

const container = document.querySelector('.container');
const count = document.getElementById('count');
const total = document.getElementById('total');
const movieSelect = document.getElementById('movie');

let ticketPrice = +movieSelect.value;

// Construye de forma dinámica los asientos de la sala para la película
// seleccionada, dando a cada uno la clase adecuada y usando los siguientes
// parámetros:
// sides: número de asientos en las alas laterales.
// center: número de asientos en la zona central.
// rows: número de filas.
//
// Para simplificar el ejercicio, marco como ocupados todos los asientos
// de la columna 2.
function createSeats(sides, center, rows) {
    const seatsPerRow = 2 * sides + center;

    for (let row = 0; row < rows; row++) {
        const rowDiv = document.createElement('div');
        rowDiv.classList.add('row');

        for (let column = 0; column < seatsPerRow; column++) {
            const seatDiv = document.createElement('div');
            seatDiv.classList.add('seat');

            if (column + 1 == sides) {
                seatDiv.classList.add('isle-seat-left');
            }

            if (column == sides + center) {
                seatDiv.classList.add('isle-seat-right');
            }

            if (column == 2) {
                seatDiv.classList.add('occupied');
            }

            rowDiv.append(seatDiv);
        }

        container.append(rowDiv);
    }
}

function populateUI() {
    const seats = [...document.querySelectorAll('.row .seat:not(.occupied)')];

    const savedMovieIndex = localStorage.getItem('selectedMovieIndex');
    if (savedMovieIndex != null) {
        movieSelect.selectedIndex = savedMovieIndex;
    }

    const savedSelectedSeats = JSON.parse(
        localStorage.getItem('selectedSeats')
    );
    if (savedSelectedSeats != null) {
        savedSelectedSeats.forEach(function (index) {
            seats[index].classList.add('selected');
        });
    }
}

function setMovieData(movieIndex) {
    localStorage.setItem('selectedMovieIndex', movieIndex);
}

function updateCount() {
    const seats = [...document.querySelectorAll('.row .seat:not(.occupied)')];
    const selectedSeats = document.querySelectorAll('.row .seat.selected');
    const selectedSeatsIndex = [...selectedSeats].map(function (seat) {
        return seats.indexOf(seat);
    });

    ticketPrice = movieSelect.value;

    localStorage.setItem('selectedSeats', JSON.stringify(selectedSeatsIndex));
    count.innerText = selectedSeats.length;
    total.innerText = selectedSeats.length * ticketPrice;
}

movieSelect.addEventListener('change', function (e) {
    ticketPrice = +e.target.value;
    setMovieData(e.target.selectedIndex);
    updateCount();
});

container.addEventListener('click', function (e) {
    if (
        e.target.classList.contains('seat') &&
        !e.target.classList.contains('occupied')
    ) {
        e.target.classList.toggle('selected');
        updateCount();
    }
});

createSeats(4, 8, 6);
populateUI();
updateCount();
