/**
 * Validador de formularios.
 * Script principal mostrando uso de librería implementada.
 * Chakir Mrabet <cmrabet@uoc.edu>, 2020.
 * PEC 1/1, Desarrollo fron-end con frameworks JavaScript.
 * Master en desarrollo de sitios y applicaciones web, UOC.
 */

// Importamos la librería de validación.
import validateForm from './validateForm';

// Definimos el schema del formulario a validar.
const formModel = [
    {
        id: 'username',
        validators: ['required', 'min:6', 'max:15'],
    },
    {
        id: 'email',
        validators: ['required', 'email'],
    },
    {
        id: 'password',
        validators: [
            'required',
            'min:8',
            'max:30',
            'regex:(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{1,}$:Numbers, upper & lower case letters, and a symbol.',
        ],
    },
    {
        id: 'password2',
        validators: ['required', "match:password:Passwords don't match"],
    },
];

// Llamamos la función de validación con el schema que hemos definido
// y el id del formulario que queremos validar.
validateForm('form', formModel, function (values) {
    // Si el formulario pasa la validación, el objeto "values" contendrá
    // los valores de cada control de formulario.
    alert('The submitted values are:' + JSON.stringify(values));
});
