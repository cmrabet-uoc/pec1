/**
 * Validador de formularios.
 * Este script implementa una función que valida formularios
 * usando un schema o modelo que describe reglas de validación.
 *
 * Chakir Mrabet <cmrabet@uoc.edu>, 2020.
 * PEC 1/1, Desarrollo fron-end con frameworks JavaScript.
 * Master en desarrollo de sitios y applicaciones web, UOC.
 */

// Valida un formulario y ejecuta una función callback en el caso
// de que la validación no tenga errores.
// formId: es el id del elemento formulario a validar.
// schema: objeto que define las reglas de validación.
// onSubmit: función a llamar si la validación no tiene errores.
export default function validateForm(formId, schema, onSubmit) {
    // Referencia al elemento formulario a validar.
    const formEl = document.getElementById(formId);

    // Almacena los valores de cada uno de los controles del formulario.
    const formValues = {};

    // Almacena los errores producidos al validar.
    let formErrors = [];

    // E V E N T O S

    // Por cada regla de validación en el schema, registra un nuevo
    // evento "change" en el control de formulario afectado.
    schema.forEach(function (rule) {
        const input = document.getElementById(rule.id);
        if (input) {
            input.addEventListener('change', function () {
                formValues[rule.id] = input.value;
                validateFormControl(input, rule.validators);
            });
            return;
        }
        console.error(
            `validateForm(): The current DOM doesn't contain any element with id "${rule.id}". Please, check your schema definition.`
        );
    });

    // Cuando se envía el formulario.
    formEl.addEventListener('submit', function (e) {
        e.preventDefault();

        // Hay que validar también en el caso de que no
        // se haya hecho antes (por ejemplo, el usuario pulsa el
        // el botón de envío sin haber rellenado nada).
        schema.forEach(function (rule) {
            const input = document.getElementById(rule.id);
            const validators = rule.validators;
            validateFormControl(input, validators);
        });

        // Si no hay errores, ejecutamos la callback pasada como argumento
        // con los valores de cada uno de los controles del formulario.
        if (!formErrors.length && onSubmit) {
            onSubmit(formValues);
        }
    });

    // F U N C I O N E S   D E   C O N T R O L   D E   U I

    // Almacena un error cuando se falla una validación.
    // formControlEl: referencia al control de formulario que falló la validación.
    // validatorName: nombre de la regla de validación.
    // errorMsg: mensaje de error a enseñar.
    function addError(formControlEl, validatorName, errorMsg) {
        // Si el error ya estaba, no lo añadimos.
        const foundError = formErrors.find(
            (formError) =>
                formError.id === formControlEl.id &&
                formError.validatorName === validatorName
        );

        if (foundError) {
            return;
        }

        // El error es nuevo.
        formErrors.push({
            id: formControlEl.id,
            validatorName,
            errorMsg,
        });
    }

    // Quita un error de la lista cuando se pasa la validación.
    // formControlEl: referencia al control de formulario que falló la validación.
    // validatorName: nombre de la regla de validación.
    function removeError(formControlEl, validatorName) {
        formErrors = formErrors.filter(
            (formError) =>
                !(
                    formError.id == formControlEl.id &&
                    formError.validatorName == validatorName
                )
        );
    }

    function updateFormControlState(formControlEl) {
        const formControlParentEl = formControlEl.parentElement;
        const formControlErrors = formErrors.filter(
            (formError) => formError.id === formControlEl.id
        );

        if (formControlErrors.length) {
            formControlParentEl.classList.remove('success');
            formControlParentEl.classList.add('error');

            const errorMsgEl = formControlParentEl.querySelector('small');
            errorMsgEl.innerText = formControlErrors[0].errorMsg;

            return;
        }

        formControlParentEl.classList.remove('error');
        formControlParentEl.classList.add('success');
    }

    // V A L I D A D O R E S

    function checkRequired(inputEl) {
        if (inputEl.value.trim() === '') {
            addError(
                inputEl,
                'required',
                `${getFieldName(inputEl)} is required`
            );
        } else {
            removeError(inputEl, 'required');
        }
    }

    function checkMinLength(inputEl, min) {
        const inputLength = inputEl.value.length;
        const inputName = getFieldName(inputEl);

        if (inputLength < min) {
            addError(
                inputEl,
                'min',
                `${inputName} must be at least ${min} characters`
            );
            return;
        }

        removeError(inputEl, 'min');
    }

    function checkMaxLength(inputEl, max) {
        const inputLength = inputEl.value.length;
        const inputName = getFieldName(inputEl);

        if (inputLength > max) {
            addError(
                inputEl,
                'max',
                `${inputName} must be less than ${max} characters`
            );
            return;
        }

        removeError(inputEl, 'max');
    }

    function checkEmail(inputEl) {
        const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if (!testRegex(re, inputEl.value.trim())) {
            addError(
                inputEl,
                'email',
                `${getFieldName(inputEl)} is not a valid email`
            );
        } else {
            removeError(inputEl, 'email');
        }
    }

    function checkMatch(inputId, input2, error) {
        const input = document.getElementById(inputId);
        if (input.value === input2.value) {
            removeError(input2, 'match');
            return;
        }
        addError(
            input2,
            'match',
            error ||
                `${getFieldName(input)} doesn't match ${getFieldName(input2)}`
        );
    }

    function checkRegex(input, reg, error) {
        if (testRegex(reg, input.value)) {
            removeError(input, 'regex');
            return;
        }
        addError(input, 'regex', error || 'Wrong format');
    }

    // F U N C I O N E S   D E   S O P O R T E

    function getFieldName(inputEl) {
        return inputEl.id.charAt(0).toUpperCase() + inputEl.id.slice(1);
    }

    // Comprueba si el valor dado satisface la expresión regular dada.
    // reg: expresión regular.
    // value: valor a comprobar.
    function testRegex(reg, value) {
        return new RegExp(reg).test(value);
    }

    // Valida el control de formulario dado con una lista de validadores.
    // inputEl: referencia al control de formulario a validar.
    // validators: array con reglas de validación.
    function validateFormControl(inputEl, validators) {
        validators.forEach(function (validator) {
            const [name, ...params] = validator.split(':');

            switch (name) {
                case 'required':
                    checkRequired(inputEl);
                    break;
                case 'min':
                    checkMinLength(inputEl, params[0]);
                    break;
                case 'max':
                    checkMaxLength(inputEl, params[0]);
                    break;
                case 'email':
                    checkEmail(inputEl);
                    break;
                case 'match':
                    checkMatch(params[0], inputEl, params[1]);
                    break;
                case 'regex':
                    checkRegex(inputEl, params[0], params[1]);
                    break;
                default:
                    console.warn(
                        `validateForm(): Validator "${name}" is not recognized, ignoring. Please, check your schema definition for DOM element with id "${inputEl.id}".`
                    );
            }
        });

        updateFormControlState(inputEl);
    }
}
