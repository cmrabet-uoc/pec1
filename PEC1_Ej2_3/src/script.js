/**
 * Calculadora de cambio de moneda.
 * Script principal.
 * Chakir Mrabet <cmrabet@uoc.edu>, 2020.
 * PEC 1/3, Desarrollo fron-end con frameworks JavaScript.
 * Master en desarrollo de sitios y applicaciones web, UOC.
 */

// Esto es para Parcel, para resolver problema explicado aqui:
// https://flaviocopes.com/parcel-regeneratorruntime-not-defined/
import 'regenerator-runtime/runtime';

// I N I C I A L I Z A C I Ó N

const apiEndPoint = 'https://api.exchangerate-api.com/v4/latest';

// Referencia al selector de moneda principal.
const currencyOneSelectEl = document.getElementById('currency1');

// Referencia al selector de moneda secundario.
const currencyTwoSelectEl = document.getElementById('currency2');

// Referencia al elemento input que conteiene el montante de moneda
// principal a cambiar a moneda secundaria.
const amountOneEl = document.getElementById('amount1');

// Referencia el elemento input que contiene el resultado del cambio.
const amountTwoEl = document.getElementById('amount2');

// Referencia a la etiqueta que muestra la tasa de cambio entre las
// monedas principal y secundaria seleccionadas.
const rateEl = document.getElementById('rate');

// Referencia al botón de alternación de moneda principal a secundaria.
const swapButtonEl = document.getElementById('swap');

// Almacena la última fecha en que la API actualizó sus datos, así
// como todas las tasas de cambio para la moneda principal seleccionada.
let exchangeRates = {
    date: new Date(),
};

// A R R A N Q U E

calculate();

// E V E N T O S

// Cuando se selecciona una nueva moneda principal.
currencyOneSelectEl.addEventListener('change', calculate);

// Cuando se selecciona una nueva moneda secundaria.
currencyTwoSelectEl.addEventListener('change', calculate);

// Cuando se cambia el montante de la moneda principal a cambiar.
amountOneEl.addEventListener('input', calculate);

// Cuando se pulsa el botón de alternación de moneda.
swapButtonEl.addEventListener('click', function (e) {
    const temp = currencyOneSelectEl.value;
    currencyOneSelectEl.value = currencyTwoSelectEl.value;
    currencyTwoSelectEl.value = temp;
    calculate();
});

// F U N C I O N E S   D E   S O P O R T E

// Calcula el nuevo montante utilizando la tasa de cambio entre las
// monedas principal y secundaria seleccionadas. También actualiza
// el indicador de tasa de cambio.
async function calculate() {
    const cOne = currencyOneSelectEl.value;

    // Consultamos la API con la moneda principal seleccionada para
    // recibir sus tasas de cambio. Para no sobrepasar la tasa de
    // consultas que nos permite la API, solo hacemos una query
    // a la misma si:
    // 1) La moneda seleccionada no ha sido "cacheada" antes.
    // 2) La antigüedad de las monedas "cacheadas" es mayor a 24 horas.

    const shouldIfetch = !(
        exchangeRates.hasOwnProperty(cOne) ||
        hoursBetween(new Date(exchangeRates['date']), new Date()) > 24
    );

    if (shouldIfetch) {
        try {
            // Mostramos estado de carga antes de hacer llamada asíncrona.
            toggleLoader();
            const res = await fetch(`${apiEndPoint}/${cOne}`);

            if (res.ok) {
                const data = await res.json();
                exchangeRates['date'] = data.date;
                exchangeRates[cOne] = data.rates;
            } else {
                // Gestionado de errores no relacionados a la accesibilidad al endpoint.
                let msg = 'Your request could not be processed.';
                if (res.status == 404) {
                    msg += `Currency ${cOne} not found.`;
                }
                // Mostramos diálogo con el error devuelo de la llamada a
                // la API de moneda.
                showError(msg);
            }

            // Ocultamos el estado de carga.
            toggleLoader();
        } catch (e) {
            // Gestionado de errores relacionados con la accesibilidad al endpoint.
            showError(`The server cannot be reached. ${e.message}`);
        }
    }

    // Acualizamos controles.
    const cTwo = currencyTwoSelectEl.value;
    const nOne = amountOneEl.value;
    const rate = exchangeRates[cOne][cTwo];
    amountTwoEl.value = (rate * nOne).toFixed(2);
    rateEl.innerText = `1 ${cOne} = ${rate} ${cTwo}`;
}

// Enciende o apaga el indicador de carga (imagen girando).
function toggleLoader() {
    const img = document.getElementById('money-img');
    img.classList.toggle('loading');
}

// Muestra un diálogo modal de error con el mensaje dado.
function showError(text) {
    const errorBox = document.getElementById('error-box');
    const body = document.querySelector('#error-box .body');
    const acceptButton = document.querySelector('#error-box .actions button');

    errorBox.classList.add('visible');
    body.innerText = text;
    acceptButton.addEventListener('click', function (e) {
        errorBox.classList.remove('visible');
    });
}

// Calcula la diferencia en horas entre dos objetos Date.
function hoursBetween(date1, date2) {
    // La diferencia entre dos objetos Date devuelve milisegundos, por tanto,
    // hay que dividir por (60 * 60 * 1000) para obtener horas.
    return Math.abs(date1 - date2) / 36e5;
}
