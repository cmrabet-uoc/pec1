/**
 * Reserva de asientos de cine 2.
 * Script principal.
 * Chakir Mrabet <cmrabet@uoc.edu>, 2020.
 * PEC 1/4, Desarrollo fron-end con frameworks JavaScript.
 * Master en desarrollo de sitios y applicaciones web, UOC.
 */

import exchangeCurrency from './exchange';
import showError from './showError';

// I N I C I A L I Z A C I Ó N

// Referencia al elemento div que contiene los asientos del cine.
const theaterEl = document.querySelector('.theater');

// Referencia al selector de moneda.
const currencySelectEl = document.getElementById('currency');

// Referencia al selector de película.
const movieSelectEl = document.getElementById('movie');

// Referencia a la etiqueta que muestra el número de asientos seleccionados.
const countEl = document.getElementById('count');

// Referencia a la etiqueta que muestra el coste total de los asientos seleccionados.
const totalEl = document.getElementById('total');

// Referencia a la etiqueta que muestra el nombre de la moneda seleccionada.
const currencyNameEl = document.getElementById('currency-name');

// Almacena la moneda seleccionada.
let currency;

// Almacena el tipo de cambio seleccionado.
let exchangeRate;

// Almacena el precio de la película seleccionada.
let ticketPrice;

// Almacena la oferta del cine (películas disponibles e información de las salas en las que se proyectan).
// En una aplicación real, esta información sería recibida desde una API o backend.
const moviesDB = [
    {
        // En lugar de usar los índices de un array, prefiero usar identificadores, lo cual permite
        // reemplazar o alterar la oferta de películas sin temor a reemplazar índices anteriores.
        id: 'avengers',
        name: 'Avengers: Endgame',
        price: 10,
        // Guarda información sobre la sala en la que se proyecta cada película.
        // > size: es un array que contiene el número de asientos en alas laterales de la sala, número de
        // asientos en la zona central de la sala, y número de filas.
        // > occupied: es un array que contiene los índices de los asientos que están ocupados.
        theater: { size: [4, 6, 5], occupied: [33, 34, 35, 36] },
    },
    {
        id: 'joker',
        name: 'Joker - Theater 1',
        price: 12,
        theater: {
            size: [5, 8, 6],
            occupied: [83, 84, 35, 55, 56],
        },
    },
    {
        id: 'toy_story_4',
        name: 'Toy Story 4 - Theater 2',
        price: 8,
        theater: { size: [5, 10, 10], occupied: [100, 101, 126, 148, 149] },
    },
    {
        id: 'lion_king',
        name: 'The Lion King - Theater 2A',
        price: 9,
        theater: { size: [4, 6, 5], occupied: [59, 60, 61, 62] },
    },
];

// A R R A N Q U E

renderUI();
updateCount();

// E V E N T O S

// Cuando el selector de moneda cambia.
currencySelectEl.addEventListener('change', async function (e) {
    try {
        // Mostramos estado de carga antes de hacer llamada asíncrona.
        toggleLoader();

        // Consultamos la api de cambio.
        exchangeRate = (await exchangeCurrency(e.target.value)).toFixed(2);

        // Actualizamos y almacenamos la nueva moneda y cambio
        // seleccionados.
        currency = e.target.value;
        localStorage.setItem('selectedCurrency2', e.target.value);
        localStorage.setItem('selectedExchangeRate2', exchangeRate);

        // Volvemos a construir la lista de películas con el nuevo
        // cambio, y actualizamos el coste total de los asientos
        // seleccionados.
        renderMovieSelect(getSavedMovie().id);
        updateCount();
    } catch (e) {
        // Mostramos diálogo con el error devuelo de la llamada a
        // la API de moneda.
        showError(e.message);
    }

    // Ocultamos el estado de carga.
    toggleLoader();
});

// Cuando el selector de película cambia.
movieSelectEl.addEventListener('change', function (e) {
    // Obtenemos los datos de la película con el id seleccionado
    // en la lista.
    const selectedMovie = moviesDB.find((movie) => movie.id === e.target.value);

    if (selectedMovie) {
        // Actualizamos y almacenamos el precio de la película seleccionada.
        ticketPrice = selectedMovie.price;
        localStorage.setItem('selectedMovieId2', selectedMovie.id);

        // Construimos los asientos de nuevo con los datos de sala de la
        // película seleccionada y los asientos seleccionados.
        renderSeats(selectedMovie.theater, getSavedSeats(selectedMovie.id));

        // Actualizamos contadores.
        updateCount();
    }
});

// Cuando se hace click en el cuerpo del elemento div que contiene los asientos.
theaterEl.addEventListener('click', function (e) {
    if (e.target.classList.contains('seat') && !e.target.classList.contains('occupied')) {
        e.target.classList.toggle('selected');
        saveSelectedSeats();
        updateCount();
    }
});

// F U N C I O N E S   D E   S O P O R T E

// Activa/desactiva estado de carga o espera (cuando la UI está esperando
// recibir la tasa de cambio para la moneda seleccionada).
function toggleLoader() {
    // Básicamente añadimos o quitamos la clase "loading" del elemento utilizado
    // como pantalla. Desde CSS entonces utilizamos esta clase para mostrar
    // lo que queramos, que en este caso es el texto "Loading..."" (mirar el HTML
    // para más información).
    const screenEl = document.querySelector('.screen');
    screenEl.classList.toggle('loading');
}

// Construye de forma dinámica los asientos de la sala para la película
// seleccionada, dando a cada uno la clase adecuada, dependiendo de si
// el asiento está disponible, seleccionado, u ocupado.
function renderSeats(theater, selectedSeats) {
    const [sides, center, rows] = theater.size;
    const seatsPerRow = 2 * sides + center;

    theaterEl.innerHTML = '';

    let index = 0;

    for (let row = 0; row < rows; row++) {
        const rowDiv = document.createElement('div');
        rowDiv.classList.add('row');

        for (let column = 0; column < seatsPerRow; column++) {
            const seatDiv = document.createElement('div');
            seatDiv.classList.add('seat');

            if (selectedSeats && includes(selectedSeats, index)) {
                seatDiv.classList.add('selected');
            }

            if (includes(theater.occupied, index)) {
                seatDiv.classList.add('occupied');
            }

            // Las clases isle-seat-left y isle-seat-right se usan para
            // indicar en qué lugar caen los pasillos.
            if (column + 1 == sides) {
                seatDiv.classList.add('isle-seat-left');
            }

            if (column == sides + center) {
                seatDiv.classList.add('isle-seat-right');
            }

            rowDiv.append(seatDiv);
            index++;
        }

        theaterEl.append(rowDiv);
    }
}

// Construye de forma dinámica el contenido del selector de películas.
function renderMovieSelect(selectedMovieId) {
    movieSelectEl.innerHTML = '';
    moviesDB.forEach(({ id, name, price }) => {
        const optionEl = document.createElement('option');
        optionEl.value = id;

        // Al definir el texto de cada opción, incluímos el precio de
        // forma dinámica aplicando la tasa de cambio seleccionada.
        optionEl.innerText = `${name} (${(price * exchangeRate).toFixed(2)} ${currency})`;

        if (selectedMovieId === id) {
            optionEl.selected = true;
        }

        movieSelectEl.append(optionEl);
    });
}

// Función principal para construir la interfaz.
function renderUI() {
    // Leemos la moneda guardada por última vez en el almacenamiento local
    // y la aplicamos al selector de moneda. Si es la primera vez, usamos
    // el dolar, USD.
    currency = localStorage.getItem('selectedCurrency2') || 'USD';
    currencySelectEl.value = currency;

    // Leemos la tasa de cambio guardada por última vez en el almacenamiento
    // local. Si es la primera vez, aplicamos cambio 1.
    exchangeRate = +localStorage.getItem('selectedExchangeRate2') || 1.0;

    // Leemos la película guardada por última vez en el alamacenamiento
    // local y construimos el selector de películas con su valor como el
    // seleccionado.
    const savedMovie = getSavedMovie();
    renderMovieSelect(savedMovie.id);

    // Almacenamos el precio actual con la película seleccionada.
    ticketPrice = savedMovie.price;

    // Construimos los asientos de la sala.
    renderSeats(savedMovie.theater, getSavedSeats(savedMovie.id));
}

// Guarda en el almacenamiento local los asientos seleccionados para cada
// película.
function saveSelectedSeats() {
    const seats = [...document.querySelectorAll('.row .seat')];
    const selectedSeats = document.querySelectorAll('.row .seat.selected');
    const selectedSeatsIndex = [...selectedSeats].map((seat) => seats.indexOf(seat));
    const seatsToSave = JSON.parse(localStorage.getItem('selectedSeats2')) || {};

    seatsToSave[movieSelectEl.value] = selectedSeatsIndex;
    localStorage.setItem('selectedSeats2', JSON.stringify(seatsToSave));
}

// Cuenta y muestra el número de asientos seleccionados para cada película, y
// actualiza la etiqueta que muestra el coste total en la moneda seleccionada.
function updateCount() {
    const selectedSeats = document.querySelectorAll('.row .seat.selected');

    countEl.innerText = selectedSeats.length;
    totalEl.innerText = (selectedSeats.length * ticketPrice * exchangeRate).toFixed(2);
    currencyNameEl.innerText = currency;
}

// Helper que devuelve la película guardada en el almacenamiento local.
function getSavedMovie() {
    const selectedMovieId = localStorage.getItem('selectedMovieId2') || moviesDB[0].id;
    return moviesDB.find((movie) => movie.id === selectedMovieId);
}

// Helper que devuelve los asientos seleccionados guardados en el almacenamiento local
// para la pélicula con el id pasado como argumento.
function getSavedSeats(movieId) {
    const savedSelectedSeats = JSON.parse(localStorage.getItem('selectedSeats2')) || {};
    if (savedSelectedSeats.hasOwnProperty(movieId)) {
        return savedSelectedSeats[movieId];
    }
}

// Helper que emula Array.includes(), método no soportado por IE.
function includes(arr, value) {
    return arr.indexOf(value) > -1;
}
