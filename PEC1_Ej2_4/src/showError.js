/**
 * Reserva de asientos de cine 2.
 * Funciones de visualización de diálogos modales.
 * Chakir Mrabet <cmrabet@uoc.edu>, 2020.
 * PEC 1/4, Desarrollo fron-end con frameworks JavaScript.
 * Master en desarrollo de sitios y applicaciones web, UOC.
 */

/**
 * Muestra un diálogo modal con el texto pasado como argumento.
 * Esta función necesita markup HTML para funcionar (mirar
 * estructura del elemento div#error-box en el documento HTML
 * para más información).
 */
export default function showError(text) {
    const errorBox = document.getElementById('error-box');
    const body = document.querySelector('#error-box .body');
    const acceptButton = document.querySelector('#error-box .actions button');

    if (errorBox && body && acceptButton) {
        errorBox.classList.add('visible');
        body.innerText = text;
        acceptButton.addEventListener('click', function (e) {
            errorBox.classList.remove('visible');
        });
        return;
    }

    console.warn(
        'showError: No div element with id "error-box" found, or its HTML structure is not correct. Please, check your HTML markup.'
    );
}
