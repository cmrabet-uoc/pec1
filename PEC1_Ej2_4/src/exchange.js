/**
 * Reserva de asientos de cine 2.
 * Funciones para consultas API.
 * Chakir Mrabet <cmrabet@uoc.edu>, 2020.
 * PEC 1/4, Desarrollo fron-end con frameworks JavaScript.
 * Master en desarrollo de sitios y applicaciones web, UOC.
 */

// Esto es para Parcel, para resolver problema explicado aqui:
// https://flaviocopes.com/parcel-regeneratorruntime-not-defined/
import 'regenerator-runtime/runtime';

// Almacena el endpoint de la API a utilizar para la consulta de
// de tasa de cambios.
const apiEndPoint = 'https://api.exchangerate-api.com/v4/latest';

// Contiene las tasas de cambio de la moneda seleccionada.
let exchangeRates = {
    // Fecha de la última actualización de cambios.
    date: new Date(),
};

// Devuelve objeto con tasas de cambio para la momenta pasada
// como referencia (ISO).
// Solo hacemos una query a la API si:
// 1) La moneda seleccionada no ha sido cacheada antes.
// 2) La antigüedad de las tasas de cambio cacheadas es mayor a 24 horas.

export default async function exchangeCurrency(cTarget) {
    const cOne = 'USD';

    const shouldIfetch = !(
        exchangeRates.hasOwnProperty('USD') ||
        hoursBetween(new Date(exchangeRates['date']), new Date()) > 24
    );

    if (shouldIfetch) {
        try {
            const res = await fetch(`${apiEndPoint}/${cOne}`);
            if (res.ok) {
                const data = await res.json();
                exchangeRates['date'] = data.date;
                exchangeRates[cOne] = data.rates;
            } else {
                throw new Error(e.message);
            }
        } catch (e) {
            // Gestionado de errores relacionados con la accesibilidad al endpoint.
            throw new Error(e.message);
        }
    }

    if (exchangeRates[cOne].hasOwnProperty(cTarget)) {
        return exchangeRates[cOne][cTarget];
    }
    throw new Error(`Currency ${cTarget} not found.`);
}

// Calcula la diferencia en horas entre dos objetos Date.
function hoursBetween(date1, date2) {
    // La diferencia entre dos objetos Date devuelve milisegundos, por tanto,
    // hay que dividir por (60 * 60 * 1000) para obtener horas.
    return Math.abs(date1 - date2) / 36e5;
}
