# Reserva de asientos de cine 2 - PEC 1/4

Desarrollo front-end con frameworks JavaScript.

2020 Chakir Mrabet - Master en desarrollo de sitios y aplicaciones web - UOC.

## Introducción

Este proyecto implementa una interfaz de compra de entradas de asientos de cine, combinando y mejorando el código de las PEC 1/2 y 1/3.

Ente otras funcionalidades, el proyecto permite lo siguiente:

1. Seleccionar la moneda en la que el usuario ve los precios de las películas y el coste total de asientos seleccionados (los precios base de las películas están en dólares americanos).
1. Seleccionar diferentes películas, cada una con una sala determinada (diferentes layouts de asientos).
1. Seleccionar diferentes asientos para cada película.
1. Recibir notificaciones cuando se produce un error al consultar la tasa de cambio de la moneda seleccionada.

Se puede acceder a una versión online [aquí](https://pec1-ej2-4-cmrabet-desarrollo-frontend.netlify.app).

## Herramientas utilizadas

El proyecto está montado con el empaquetador [Parcel](https://parceljs.org/). La configuración necesaria para hacerlo funcionar se encuentra en el archivo `package.json`.

Para asegurar compatibilidad entre navegadores al aplicar reglas CSS, utilizo el plugin [autoprefixer](https://autoprefixer.github.io/), el cual se encarga de transpilar el CSS implementado añadiendo los prefijos necesarios para cada navegador web que satisfaga las condiciones descritas en el archivo `.browserslistrc`.

En el código JavaScript sigo las siguientes convenciones:

1. Nombro todas las referencias a elementos DOM con el sufijo `el`.
1. Hago uso de funciones de flecha (_fat arrow function_) solo para los callbacks de operaciones sobre arrays. Para el resto de funciones, utilizo la forma completa para mantener legibilidad de código y facilitar debuggeo.
1. Utilizo `import` para separar el código en diferentes archivos. [Parcel](https://parceljs.org/) entiende como procesar este keyword para empaquetar todo el JavaScript resultante en un solo archivo.
1. Utilizo el método `Object.hasOwnProperty()` para averiguar si una propiedad existe en un objeto. Este método está soportado por todos los navegadores actuales modernos.
    1. [Can I use Object.hasOwnProperty?](https://caniuse.com/mdn-javascript_builtins_object_hasownproperty)

## Instrucciones de uso

Para arrancar el proyecto en modo desarrollo, ejecutar los siguientes comandos desde el directorio raíz del proyecto:

`npm install`

`npm run dev`

Eso hará que [Parcel](https://parceljs.org/) levante un servidor web de desarrollo automáticamente.

Para compilar la versión de producción, ejecutar el siguiente comando:

`npm run build`

Esto hará que [Parcel](https://parceljs.org/) cree una carpeta llamada `dist` con los archivos necesarios para distribuir la aplicación.

## Detalles de implementación

El código está distribuído en tres archivos:

1. `src/script.js`: script principal de entrada.
1. `src/exchange.js`: librería para llamadas a la API de cambio de moneda.
1. `src/showError.js`: librería para mostrar un dialogo modal de error.

Las principales mejoras introducidas con respecto a las PECs anteriores son:

1. Cada película puede tener un número diferente de asientos (o salas), lo cual almaceno en el objeto `moviesDB`. Me hubiera gustado implementar un backend mínimo con `node.js` que sirva estos datos, pero por falta de tiempo no he podido.
1. Los asientos son creados dinamicamente en el DOM en base a la sala que tiene la película elegida.
1. Los asientos seleccionados son almacenados por película.
1. He añadido notificaciones para cuando hay errores al hacer consultas a la API.
